package fr.dejamobile.nbroquet.dejaquiz.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.dejamobile.nbroquet.dejaquiz.repository.QuizRepository
import java.lang.Exception
import java.lang.IllegalArgumentException

/**
 * Factory use to DI the context necessary for the database + id of the current quiz used for this session
 */
class QuizSessionViewModelFactory(private val context: Context?, private val quizId: Int): ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(QuizSessionViewModel::class.java)) {
            if(context == null) {
                throw Exception("No context to create repository")
            }
            val repository = QuizRepository(context)
            return QuizSessionViewModel(repository, quizId) as T
        } else {
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}