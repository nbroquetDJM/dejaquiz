package fr.dejamobile.nbroquet.dejaquiz.view.quiz_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.ListAdapter
import fr.dejamobile.nbroquet.dejaquiz.R
import fr.dejamobile.nbroquet.dejaquiz.model.Quiz

/**
 * Adapter for the quiz list recycler view
 */
class QuizListAdapter: ListAdapter<Quiz, QuizListViewHolder>(Quiz.DiffCallback()) {
    private val _selectedQuiz: MutableLiveData<Quiz> = MutableLiveData(Quiz.EMPTY_QUIZ)
    val selectedQuiz: LiveData<Quiz>
        get() = _selectedQuiz

    /**
     * Inflate the viewholder with the QuizListViewHolder class
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuizListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.quiz_item, parent, false)
        return QuizListViewHolder(view)
    }

    /**
     * Fill viewholder with item data
     */
    override fun onBindViewHolder(holder: QuizListViewHolder, position: Int) {
        val item: Quiz = getItem(position)
        holder.quizIdView.text = "${item.id}"
        holder.quizNameView.text = item.name

        // Selected quiz listener + update UI
        holder.itemView.setOnClickListener {
            holder.itemView.isSelected = !holder.itemView.isSelected

            if(holder.itemView.isSelected) {
                _selectedQuiz.value = item
            } else {
                _selectedQuiz.value = Quiz.EMPTY_QUIZ
            }
        }
    }
}