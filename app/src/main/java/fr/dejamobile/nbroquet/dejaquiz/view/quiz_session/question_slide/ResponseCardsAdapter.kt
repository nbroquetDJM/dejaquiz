package fr.dejamobile.nbroquet.dejaquiz.view.quiz_session.question_slide

import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.ListAdapter
import com.google.android.material.card.MaterialCardView
import fr.dejamobile.nbroquet.dejaquiz.R
import fr.dejamobile.nbroquet.dejaquiz.model.Response


/**
 * Adapter for the response list recycler view
 */
class ResponseCardsAdapter(): ListAdapter<Response, ResponseCardsViewHolder>(Response.DiffCallback()) {
    // The correct response among all
    private val _correctResponse : MutableLiveData<Response> = MutableLiveData<Response>(Response.EMPTY_RESPONSE)

    // List of all responses
    private val _responseCardList : ArrayList<MaterialCardView> = arrayListOf()

    // If the user has answered
    private val _hasAnswered : MutableLiveData<Pair<Boolean, Boolean>> = MutableLiveData<Pair<Boolean, Boolean>>(Pair(
        first = false,  // yes or no
        second = false   // correct or wrong response
    ))
    val hasAnswered : LiveData<Pair<Boolean, Boolean>>
        get() = _hasAnswered

    /**
     * Inflate the viewholder with the ResponseCardsViewHolder class
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResponseCardsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.response_item,
            parent,
            false
        )
        return ResponseCardsViewHolder(view)
    }

    /**
     * Fill viewholder with item data
     */
    override fun onBindViewHolder(holder: ResponseCardsViewHolder, position: Int) {
        // Get current response
        val item: Response = getItem(position)

        // Bind response label
        holder.responseLbl.text = item.response

        // Get response card component
        val card = holder.responseCard

        // Add it to a list of all possible responses
        _responseCardList.add(card)

        // Add a click listener on the card
        card.setOnClickListener {
            // Change check state
            card.toggle()

            // Remove click listener
            _responseCardList.forEach { card ->
                card.setOnClickListener(null)
                card.isEnabled = false
            }

            // Check if the response is correct or not
            var isResponseCorrect : Boolean = false
            if(item.id == _correctResponse.value?.id) {
                isResponseCorrect = true
                setCardColorPalette(card, R.color.correct_tint, R.color.correct_ripple, R.color.correct_background,  R.drawable.ic_correct_answer)
            } else {
                isResponseCorrect = false
                setCardColorPalette(card, R.color.incorrect_tint, R.color.incorrect_ripple, R.color.incorrect_background, R.drawable.ic_wrong_answer)
            }

            // The user has answered, observed by QuestionSlideFragment
            _hasAnswered.value = Pair(true, isResponseCorrect)
        }
    }

    /**
     * Setter for correct response
     */
    fun setCorrectResponse(response: Response) {
        _correctResponse.value = response
    }

    /**
     * Set color palette for card component
     */
    private fun setCardColorPalette(card : MaterialCardView, iconTint : Int, rippleTint : Int, backgroundTint : Int, icon : Int) {
        // Icon
        card.checkedIcon = card.context.getDrawable(icon)
        card.checkedIconTint = ColorStateList.valueOf(card.context.getColor(iconTint))
        // Stroke
        card.strokeColor = card.context.getColor(iconTint)
        card.strokeWidth = 4
        // Ripple
        card.rippleColor = ColorStateList.valueOf(card.context.getColor(rippleTint))
        // Background
        card.setCardBackgroundColor(card.context.getColor(backgroundTint))
    }
}