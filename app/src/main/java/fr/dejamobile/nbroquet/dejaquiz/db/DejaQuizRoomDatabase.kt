package fr.dejamobile.nbroquet.dejaquiz.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import fr.dejamobile.nbroquet.dejaquiz.dao.QuestionDao
import fr.dejamobile.nbroquet.dejaquiz.dao.QuizDao
import fr.dejamobile.nbroquet.dejaquiz.dao.QuizSessionDao
import fr.dejamobile.nbroquet.dejaquiz.dao.ResponseDao
import fr.dejamobile.nbroquet.dejaquiz.model.Question
import fr.dejamobile.nbroquet.dejaquiz.model.Quiz
import fr.dejamobile.nbroquet.dejaquiz.model.QuizSession
import fr.dejamobile.nbroquet.dejaquiz.model.Response

/**
 * Singleton instance of the local Room database
 */
@Database(
    entities = [Quiz::class, Question::class, Response::class, QuizSession::class],
    version = 1,
    exportSchema = false
)
abstract class DejaQuizRoomDatabase: RoomDatabase() {
    abstract fun quizDao(): QuizDao
    abstract fun questionDao(): QuestionDao
    abstract fun responseDao(): ResponseDao
    abstract fun quizSessionDao(): QuizSessionDao

    companion object {
        @Volatile
        private var INSTANCE: DejaQuizRoomDatabase? = null

        /**
         * Unique INSTANCE, pre-populate directly from asset folder
         */
        fun getInstance(context: Context): DejaQuizRoomDatabase {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(context, DejaQuizRoomDatabase::class.java, "DejaQuizDB")
                        .createFromAsset("database/DejaQuizDB.db")
                        .build()

                    return INSTANCE!!
                }
            } else {
                return INSTANCE!!
            }
        }
    }
}