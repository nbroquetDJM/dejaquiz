package fr.dejamobile.nbroquet.dejaquiz.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.dejamobile.nbroquet.dejaquiz.repository.QuizRepository
import java.lang.Exception
import java.lang.IllegalArgumentException

/**
 * Factory use to DI the context necessary for the database
 */
class QuizListViewModelFactory(private val context: Context?) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(QuizListViewModel::class.java)) {
            if (context == null) {
                throw Exception("No context to create repository")
            }
            val repository = QuizRepository(context)
            return QuizListViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}