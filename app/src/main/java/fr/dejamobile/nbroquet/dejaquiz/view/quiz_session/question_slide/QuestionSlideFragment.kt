package fr.dejamobile.nbroquet.dejaquiz.view.quiz_session.question_slide

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import fr.dejamobile.nbroquet.dejaquiz.databinding.FragmentQuestionSlideBinding
import fr.dejamobile.nbroquet.dejaquiz.model.Response
import fr.dejamobile.nbroquet.dejaquiz.utils.BundleKeys
import fr.dejamobile.nbroquet.dejaquiz.viewmodel.QuizSessionViewModel
import fr.dejamobile.nbroquet.dejaquiz.viewmodel.QuizSessionViewModelFactory
import java.util.ArrayList

class QuestionSlideFragment(val quizId: Int, val questionId: Long): Fragment() {
    private lateinit var _binding: FragmentQuestionSlideBinding

    // Shared ViewModel
    private val quizSessionViewModel : QuizSessionViewModel by activityViewModels(factoryProducer = { QuizSessionViewModelFactory(context, quizId) })

    // Adapter for the Recyclerview of the responses list
    private val responseCardsAdapter = ResponseCardsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentQuestionSlideBinding.inflate(inflater)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Recycler view for responses
        _binding.responseCardsRv.apply {
            setHasFixedSize(true)
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            adapter = responseCardsAdapter
        }

        // Get arguments for the corresponding fragment instance (Question + Responses)
        arguments?.takeIf { it.containsKey(BundleKeys.QUESTION_KEY) && it.containsKey(BundleKeys.REPONSES_KEY) }?.apply {
            // Print question label
            val questionLbl = _binding.questionLbl
            questionLbl.text = getString(BundleKeys.QUESTION_KEY).toString()

            // Print responses
            val responses: ArrayList<Response>? = getParcelableArrayList(BundleKeys.REPONSES_KEY)
            responseCardsAdapter.submitList(responses)

            // Set correct response
            responseCardsAdapter.setCorrectResponse(getParcelable(BundleKeys.CORRECT_RESPONSE_KEY) ?: Response.EMPTY_RESPONSE)
        }

        // Observe if the user has answered
        responseCardsAdapter.hasAnswered.observe(viewLifecycleOwner) { answered ->
            if(answered.first) {
                quizSessionViewModel.answerQuestion(questionId, answered.second)
            }
        }
    }
}