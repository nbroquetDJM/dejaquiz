package fr.dejamobile.nbroquet.dejaquiz.dao

import androidx.room.Dao
import androidx.room.Query

/**
 * Requests used for the table Question
 */
@Dao
interface QuestionDao {
    /**
     * Get number of question in a quiz
     */
    @Query("SELECT COUNT(quizId) FROM Question WHERE quizId = :quizId")
    fun getQuestionCount(quizId: Int) : Int
}