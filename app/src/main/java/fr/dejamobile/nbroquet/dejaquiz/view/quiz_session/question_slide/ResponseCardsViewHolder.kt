package fr.dejamobile.nbroquet.dejaquiz.view.quiz_session.question_slide

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import fr.dejamobile.nbroquet.dejaquiz.R

/**
 * ViewHolder for a response item of the response list recyclerview
 */
class ResponseCardsViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val responseCard: MaterialCardView = view.findViewById(R.id.response_card)
    val responseLbl: TextView = view.findViewById(R.id.response_lbl)
}