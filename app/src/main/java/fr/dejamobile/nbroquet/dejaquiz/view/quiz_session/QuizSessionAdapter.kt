package fr.dejamobile.nbroquet.dejaquiz.view.quiz_session

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import fr.dejamobile.nbroquet.dejaquiz.model.QuizWithQuestionsAndResponses
import fr.dejamobile.nbroquet.dejaquiz.model.Response
import fr.dejamobile.nbroquet.dejaquiz.utils.BundleKeys
import fr.dejamobile.nbroquet.dejaquiz.view.quiz_session.question_slide.QuestionSlideFragment
import java.util.ArrayList

/**
 * Adapter for the question view pager
 */
class QuizSessionAdapter(fa: FragmentActivity, private val quizData: LiveData<QuizWithQuestionsAndResponses>): FragmentStateAdapter(fa) {
    /**
     * Number of questions
     */
    override fun getItemCount(): Int {
        val numberOfQuestion = quizData.value?.questions?.size
        return numberOfQuestion ?: 0
    }

    /**
     * Create a fragment for each question & put data (Question + Responses) within a bundle
     */
    override fun createFragment(position: Int): Fragment {
        quizData.value.let {
            val fragment = QuestionSlideFragment(it?.quiz?.id ?: 0, it?.questions?.get(position)?.question?.id ?: 0)
            fragment.arguments = Bundle().apply {
                // Question label
                val question = it?.questions?.get(position)?.question
                putString(BundleKeys.QUESTION_KEY, question?.question ?: "")
                // Responses
                val responses = it?.questions?.get(position)?.responses
                putParcelableArrayList(BundleKeys.REPONSES_KEY, responses as ArrayList<Response>)
                // Correct response
                val correctResponse = responses.find { resp -> resp.id == question?.correctResponseId }
                putParcelable(BundleKeys.CORRECT_RESPONSE_KEY, correctResponse)
            }
            return fragment
        }
    }
}