package fr.dejamobile.nbroquet.dejaquiz.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import fr.dejamobile.nbroquet.dejaquiz.model.QuizSession

/**
 * Requests used for the table Question
 */
@Dao
interface QuizSessionDao {
    /**
     * Get a quiz session by id
     */
    @Query("SELECT * FROM QuizSession WHERE id = :quizSessionId")
    fun getQuizSessionById(quizSessionId : Long) : QuizSession

    /**
     * Insert a new quiz session
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertQuizSession(quizSession: QuizSession) : Long

    /**
     * Insert a new quiz session, then get it by id
     */
    @Transaction
    suspend fun startQuizSession(quizSession: QuizSession) : QuizSession {
        val insertedId = insertQuizSession(quizSession)
        return getQuizSessionById(insertedId)
    }

    /**
     * Update score of the current session
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateScore(quizSession: QuizSession)

    /**
     * End the quiz session
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun endQuizSession(quizSession: QuizSession)
}