package fr.dejamobile.nbroquet.dejaquiz.view.quiz_session

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import androidx.navigation.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.transition.MaterialSharedAxis
import fr.dejamobile.nbroquet.dejaquiz.R
import fr.dejamobile.nbroquet.dejaquiz.databinding.FragmentQuizSessionBinding
import fr.dejamobile.nbroquet.dejaquiz.model.QuizSession
import fr.dejamobile.nbroquet.dejaquiz.viewmodel.QuizSessionViewModel
import fr.dejamobile.nbroquet.dejaquiz.viewmodel.QuizSessionViewModelFactory
import java.text.SimpleDateFormat
import java.util.*

class QuizSessionFragment:FragmentActivity() {
    private lateinit var _binding: FragmentQuizSessionBinding

    // We don't use by viewModels keyword because we need to have access to this ViewModel everywhere in this class.
    // And also we need SafeArgs during to initialize the Factory
    private lateinit var quizSessionViewModel: QuizSessionViewModel
    private lateinit var quizSessionViewModelFactory: QuizSessionViewModelFactory

    private lateinit var viewPager: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = FragmentQuizSessionBinding.inflate(layoutInflater)
        setContentView(_binding.root)

        // Transitions
        /*supportFragmentManager.fragments.forEach { fragment ->
            fragment.enterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true).apply {
                duration = resources.getInteger(R.integer.reply_motion_duration_large).toLong()
            }

            fragment.returnTransition = MaterialSharedAxis(MaterialSharedAxis.Z, false).apply {
                duration = resources.getInteger(R.integer.reply_motion_duration_large).toLong()
            }
        }*/

        // SafeArgs, get quiz id + username
        val args: QuizSessionFragmentArgs by navArgs()
        val quizId = args.quizId
        val username = args.username

        // ViewModel
        quizSessionViewModelFactory = QuizSessionViewModelFactory(this, quizId)
        quizSessionViewModel = ViewModelProvider(this, quizSessionViewModelFactory).get(QuizSessionViewModel::class.java)

        // Get selected quiz name and update UI
        quizSessionViewModel.selectedQuiz.observe(this) { quiz ->
            _binding.quizSessionNameLbl.text = quiz.name
        }

        // Get quiz data from the DB via the viewmodel
        val quizData = quizSessionViewModel.getQuizWithQuestionsAndResponsesById(quizId)

        // When data received, start quiz and notify the recyclerview adapter
        quizData.observe(this) {
            // Start quiz session
            startQuiz(quizId, username)

            // Notifiy dataset changed
            _binding.quizSessionViewpager.adapter?.notifyDataSetChanged()
        }

        // Create an adapter for the quiz session recycler view
        val quizSessionAdapter = QuizSessionAdapter(this, quizData)
        viewPager = _binding.quizSessionViewpager
        viewPager.adapter = quizSessionAdapter

        // Add a tab layout to easily access to a particular question
        TabLayoutMediator(_binding.questionTab, viewPager) { tab, position ->
            // Tab name
            tab.text = "Question N°${position + 1}"
        }.attach()

        // Observe quiz session
        quizSessionViewModel.quizSession.observe(this) { session ->
            // Check if ended
            if (session != QuizSession.EMPTY_QUIZ_SESSION && session.finished == 1) {
                Log.d("DEJATEST", "Session.finished ${session.finished}")
                Toast.makeText(baseContext, "GameOver", Toast.LENGTH_LONG).show()
            }
        }
    }

    /**
     * Start quiz session, add new entry in database
     */
    private fun startQuiz(quizId: Int, username: String) {
        // Create a formatted date string
        val pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        val simpleDateFormat = SimpleDateFormat(pattern, Locale.ENGLISH)
        val currentData = simpleDateFormat.format(Date())

        // Create a quiz session
        val newQuizSession = QuizSession(0, currentData, username, quizId, 0, 0)

        // Insert a new quiz session in the DB
        quizSessionViewModel.startQuiz(newQuizSession)
    }

    override fun onBackPressed() {
        if (viewPager.currentItem == 0) {
            // If the user is currently looking at the first question, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed()
        } else {
            // Otherwise, select the previous question.
            viewPager.currentItem = viewPager.currentItem - 1
        }
    }
}