package fr.dejamobile.nbroquet.dejaquiz.dao

import androidx.room.Dao
import androidx.room.Query

/**
 * Requests used for the table Response
 */
@Dao
interface ResponseDao {
}