package fr.dejamobile.nbroquet.dejaquiz.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Quiz entity from the database
 */
@Entity(tableName = "QuizList")
data class Quiz(@PrimaryKey(autoGenerate = true) val id: Int, val name: String) {
    /**
     * Compare items for the recyclerview
     */
    class DiffCallback: DiffUtil.ItemCallback<Quiz>() {
        override fun areItemsTheSame(oldItem: Quiz, newItem: Quiz): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Quiz, newItem: Quiz): Boolean {
            return oldItem.id == newItem.id && oldItem.name == newItem.name
        }
    }

    companion object {
        val EMPTY_QUIZ = Quiz(0, "")
    }
}
