package fr.dejamobile.nbroquet.dejaquiz.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.dejamobile.nbroquet.dejaquiz.model.Quiz
import fr.dejamobile.nbroquet.dejaquiz.repository.QuizRepository

/**
 * ViewModel used for the quiz list
 */
class QuizListViewModel(repository : QuizRepository): ViewModel() {
    // List of all quizzes
    val quizList: LiveData<List<Quiz>> = repository.quizList

    // Chosen username
    private val _username: MutableLiveData<String> = MutableLiveData("")
    val username: LiveData<String>
        get() = _username

    // Setter for username
    fun choseUserName(username: String) {
        _username.value = username
    }
}