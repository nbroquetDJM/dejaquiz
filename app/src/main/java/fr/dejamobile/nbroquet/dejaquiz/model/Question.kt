package fr.dejamobile.nbroquet.dejaquiz.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Question entity from the database
 */
@Entity(
    tableName = "Question",
    foreignKeys = [
        ForeignKey(entity = Quiz::class, parentColumns = ["id"], childColumns = ["quizId"]),
        ForeignKey(entity = Response::class, parentColumns = ["id"], childColumns = ["correctResponseId"])
    ],
    indices = [Index("quizId"), Index("correctResponseId")]
)
data class Question(@PrimaryKey(autoGenerate = true) val id: Long, val question: String, val correctResponseId: Int, val quizId: Int) {
    /**
     * Compare items for the recyclerview
     */
    class DiffCallback: DiffUtil.ItemCallback<Question>() {
        override fun areItemsTheSame(oldItem: Question, newItem: Question): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Question, newItem: Question): Boolean {
            return oldItem.id == newItem.id && oldItem.question == newItem.question && oldItem.correctResponseId == newItem.correctResponseId && oldItem.quizId == newItem.quizId
        }
    }

    companion object {
        val EMPTY_QUESTION = Question(0, "", 0, 0)
    }
}