package fr.dejamobile.nbroquet.dejaquiz.model

import android.content.IntentSender
import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * QuizSession entity from the database
 */
@Entity(
    tableName = "QuizSession",
    foreignKeys = [
        ForeignKey(entity = Quiz::class, parentColumns = ["id"], childColumns = ["quizId"])
    ],
    indices = [Index("quizId")]
)
data class QuizSession(@PrimaryKey(autoGenerate = true) val id: Long, val date: String, val username: String?, val quizId: Int, var finished: Int = 0, var score: Int? = 0) {
    /**
     * Compare items for the recyclerview
     */
    class DiffCallback: DiffUtil.ItemCallback<QuizSession>() {
        override fun areItemsTheSame(oldItem: QuizSession, newItem: QuizSession): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: QuizSession, newItem: QuizSession): Boolean {
            return oldItem.id == newItem.id && oldItem.date == newItem.date && oldItem.username == newItem.username
                    && oldItem.quizId == newItem.quizId && oldItem.finished == newItem.finished && oldItem.score == newItem.score
        }
    }

    companion object {
        val EMPTY_QUIZ_SESSION = QuizSession(0, "0000-00-00", null, 0, 0, 0)
    }
}