package fr.dejamobile.nbroquet.dejaquiz.model

import androidx.room.Embedded
import androidx.room.Relation

/**
 * This class captures the relationship between a [Quiz] and it's [Question] with their [Response], which is
 * used by Room to fetch the related entities.
 */
data class QuizWithQuestionsAndResponses(
    @Embedded val quiz: Quiz,
    @Relation(
        entity = Question::class,
        parentColumn = "id",
        entityColumn = "quizId"
    )
    val questions: List<QuestionWithResponses>
)