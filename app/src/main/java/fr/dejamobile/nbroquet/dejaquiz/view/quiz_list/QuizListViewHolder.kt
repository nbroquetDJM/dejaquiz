package fr.dejamobile.nbroquet.dejaquiz.view.quiz_list

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.dejamobile.nbroquet.dejaquiz.R

/**
 * ViewHolder for a quiz item of the quiz list recyclerview
 */
class QuizListViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val quizIdView: TextView = view.findViewById(R.id.quiz_id_lbl)
    val quizNameView: TextView = view.findViewById(R.id.quiz_name_lbl)
}