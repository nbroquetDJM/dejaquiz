package fr.dejamobile.nbroquet.dejaquiz.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import fr.dejamobile.nbroquet.dejaquiz.model.Quiz
import fr.dejamobile.nbroquet.dejaquiz.model.QuizWithQuestionsAndResponses

/**
 * Requests used for the table QuizList
 */
@Dao
interface QuizDao {
    /**
     * Get all quizzes
     */
    @Query("SELECT * FROM QuizList")
    fun getList(): LiveData<List<Quiz>>

    /**
     * Get a quiz by id
     */
    @Query("SELECT * FROM QuizList WHERE id = :quizId")
    fun getById(quizId: Int): LiveData<Quiz>

    /**
     * Get a quiz with its questions and their responses
     */
    @Transaction
    @Query("SELECT * FROM QuizList WHERE id = :quizId")
    fun getQuizWithQuestionsAndResponsesById(quizId: Int): LiveData<QuizWithQuestionsAndResponses>
}