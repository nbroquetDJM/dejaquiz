package fr.dejamobile.nbroquet.dejaquiz.utils

object BundleKeys {
    // Bundle keys for quiz session
    const val QUESTION_KEY = "QUESTION_KEY"
    const val REPONSES_KEY = "REPONSES_KEY"
    const val CORRECT_RESPONSE_KEY = "CORRECT_RESPONSE_KEY"
}