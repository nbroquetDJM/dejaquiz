package fr.dejamobile.nbroquet.dejaquiz.model

import androidx.room.Embedded
import androidx.room.Relation

/**
 * This class captures the relationship between a [Question] and it's [Response], which is
 * used by Room to fetch the related entities.
 */
data class QuestionWithResponses(
    @Embedded val question: Question,
    @Relation(
        parentColumn = "id",
        entityColumn = "questionId"
    )
    val responses: List<Response> = emptyList()
)