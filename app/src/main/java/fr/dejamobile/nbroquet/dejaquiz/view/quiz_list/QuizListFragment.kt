package fr.dejamobile.nbroquet.dejaquiz.view.quiz_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.transition.MaterialSharedAxis
import fr.dejamobile.nbroquet.dejaquiz.R
import fr.dejamobile.nbroquet.dejaquiz.databinding.FragmentQuizListBinding
import fr.dejamobile.nbroquet.dejaquiz.model.Quiz
import fr.dejamobile.nbroquet.dejaquiz.utils.CombineUserQuiz
import fr.dejamobile.nbroquet.dejaquiz.viewmodel.QuizListViewModel
import fr.dejamobile.nbroquet.dejaquiz.viewmodel.QuizListViewModelFactory

class QuizListFragment: Fragment() {
    private lateinit var _binding: FragmentQuizListBinding

    // Add viewmodel to get quiz list data
    // We use a factory for context DI (Dependency Injection)
    private val quizViewModel : QuizListViewModel by viewModels(factoryProducer = {QuizListViewModelFactory(context)})

    // Adapter for the Recyclerview of the quiz list
    private val quizListAdapter = QuizListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentQuizListBinding.inflate(inflater)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Add Recyclerview for quiz list
        _binding.quizListRv.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = quizListAdapter
        }

        // We observe dynamic quiz list, filled with local Room database data
        quizViewModel.quizList.observe(viewLifecycleOwner) { list ->
            quizListAdapter.submitList(list)
        }

        // Get Username
        _binding.usernameEdit.doAfterTextChanged {
            quizViewModel.choseUserName(it.toString())
        }

        // Observe if we have a username and a selected quiz
        val startQuizLiveData = CombineUserQuiz(quizViewModel.username, quizListAdapter.selectedQuiz)
        startQuizLiveData.observe(viewLifecycleOwner) {
            if (it.first != "" && it.second != Quiz.EMPTY_QUIZ) {
                _binding.startQuizButton.visibility = View.VISIBLE
            } else {
                _binding.startQuizButton.visibility = View.GONE
            }
        }

        // Start quiz button listener -> Navigate to Session Quiz
        _binding.startQuizButton.setOnClickListener {
            quizListAdapter.selectedQuiz.value?.let { quiz ->
                navigateToQuizSession(quiz.id, quizViewModel.username.value ?: "anonymous")
            }
        }
    }

    /**
     * Navigate to Session Quiz Listener
     */
    private fun navigateToQuizSession(quizId: Int, username: String) {
        // Transition
        /*apply {
            exitTransition = MaterialSharedAxis(MaterialSharedAxis.Z, true).apply {
                duration = resources.getInteger(R.integer.reply_motion_duration_large).toLong()
            }
            reenterTransition = MaterialSharedAxis(MaterialSharedAxis.Z, false).apply {
                duration = resources.getInteger(R.integer.reply_motion_duration_large).toLong()
            }
        }*/

        // Navigation with selected quiz id
        val directions = QuizListFragmentDirections.actionQuizListFragmentToQuizSessionFragment(quizId, username)
        findNavController().navigate(directions)
    }
}