package fr.dejamobile.nbroquet.dejaquiz.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.google.android.material.transition.MaterialSharedAxis
import fr.dejamobile.nbroquet.dejaquiz.R
import fr.dejamobile.nbroquet.dejaquiz.view.quiz_list.QuizListFragmentDirections
import fr.dejamobile.nbroquet.dejaquiz.view.quiz_session.QuizSessionFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}