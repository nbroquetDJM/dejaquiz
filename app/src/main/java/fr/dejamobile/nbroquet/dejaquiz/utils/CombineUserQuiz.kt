package fr.dejamobile.nbroquet.dejaquiz.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import fr.dejamobile.nbroquet.dejaquiz.model.Quiz

/**
 * Class used to observe two livedata : Username and Selected Quiz
 */
class CombineUserQuiz(username: LiveData<String>, quiz: LiveData<Quiz>): MediatorLiveData<Pair<String, Quiz>>() {
    private var currentUsername : String = ""
    private var currentQuiz : Quiz = Quiz.EMPTY_QUIZ

    init {
        value = Pair(currentUsername, currentQuiz)

        addSource(username) {
            if(it != null) currentUsername = it
            value = Pair(currentUsername, currentQuiz)
        }

        addSource(quiz) {
            if(it != null) currentQuiz = it
            value = Pair(currentUsername, currentQuiz)
        }
    }
}