package fr.dejamobile.nbroquet.dejaquiz.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.dejamobile.nbroquet.dejaquiz.model.Quiz
import fr.dejamobile.nbroquet.dejaquiz.model.QuizSession
import fr.dejamobile.nbroquet.dejaquiz.model.QuizWithQuestionsAndResponses
import fr.dejamobile.nbroquet.dejaquiz.repository.QuizRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * ViewModel used for the quiz session
 */
class QuizSessionViewModel(private val repository : QuizRepository, quizId: Int): ViewModel() {
    // Selected quiz for the session
    val selectedQuiz: LiveData<Quiz> = repository.getById(quizId)

    // Quiz session, handled throughout the session
    private val _quizSession: MutableLiveData<QuizSession> = MutableLiveData(QuizSession.EMPTY_QUIZ_SESSION)
    val quizSession : LiveData<QuizSession>
        get() = _quizSession

    // Number of question
    private val _questionCount: MutableLiveData<Int> = MutableLiveData()

    // ArrayList with quiz results, for each question if the user has answered correctly
    private val _quizSessionResult: MutableLiveData<ArrayList<Pair<Long, Boolean>>> = MutableLiveData(arrayListOf())

    /**
     * Get all useful of a quiz
     */
    fun getQuizWithQuestionsAndResponsesById(quizId: Int): LiveData<QuizWithQuestionsAndResponses> {
         return repository.getQuizWithQuestionsAndResponsesById(quizId)
    }

    /**
     * Start a quiz by creating a quiz session
     */
    fun startQuiz(quizSession: QuizSession) {
        viewModelScope.launch(Dispatchers.IO) {
            // Create quiz session
            val startedQuizSession = repository.startQuizSession(quizSession)

            // Update the livedata
            _quizSession.postValue(startedQuizSession)

            // Get count of question
            _questionCount.postValue(
                startedQuizSession.quizId.let { repository.getQuestionCount(it) }
            )
        }
    }

    /**
     * The user has answered to a question, update score
     */
    fun answerQuestion(questionId : Long, correctResponse : Boolean) {
        // Update score
        if(correctResponse) {
            viewModelScope.launch(Dispatchers.IO) {
                _quizSession.value?.let { session ->
                    // Increment score
                    session.score = session.score?.plus(1)
                    // Update in DB
                    repository.updateScore(session)
                    // Update livedata
                    _quizSession.postValue(session)
                }
            }
        }

        // List of [question, is correct]
        _quizSessionResult.value?.add(Pair(questionId, correctResponse))

        // Check if GameOver
        if(_quizSessionResult.value?.size == _questionCount.value) {
            // GameOver
            viewModelScope.launch(Dispatchers.IO) {
                _quizSession.value?.let { session ->
                    // End session
                    session.finished = 1
                    // Update in DB
                    repository.endQuizSession(session)
                    // Update livedata
                    _quizSession.postValue(session)
                }
            }
        }
    }
}