package fr.dejamobile.nbroquet.dejaquiz.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import fr.dejamobile.nbroquet.dejaquiz.db.DejaQuizRoomDatabase
import fr.dejamobile.nbroquet.dejaquiz.model.Quiz
import fr.dejamobile.nbroquet.dejaquiz.model.QuizSession
import fr.dejamobile.nbroquet.dejaquiz.model.QuizWithQuestionsAndResponses

/**
 * Single source of truth
 * Expose some DAO methods
 */
class QuizRepository(context: Context) {
    // Access to all Dao
    private val quizDao = DejaQuizRoomDatabase.getInstance(context).quizDao()
    private val questionDao = DejaQuizRoomDatabase.getInstance(context).questionDao()
    private val responseDao = DejaQuizRoomDatabase.getInstance(context).responseDao()
    private val quizSessionDao = DejaQuizRoomDatabase.getInstance(context).quizSessionDao()

    // List of available quizzes
    val quizList: LiveData<List<Quiz>> = quizDao.getList()

    /**
     * Get a quiz by id
     */
    fun getById(quizId: Int): LiveData<Quiz> {
        return quizDao.getById(quizId)
    }

    /**
     * Get a quiz with its questions and their responses
     */
    fun getQuizWithQuestionsAndResponsesById(quizId: Int): LiveData<QuizWithQuestionsAndResponses> {
        return quizDao.getQuizWithQuestionsAndResponsesById(quizId)
    }

    /**
     * Get a quiz session by id
     */
    fun getQuizSessionById(quizSessionId : Long) : QuizSession {
        return quizSessionDao.getQuizSessionById(quizSessionId)
    }

    /**
     * Get number of question in a quiz
     */
    fun getQuestionCount(quizId: Int) : Int {
        return questionDao.getQuestionCount(quizId)
    }

    /**
     * Start a quiz session
     */
    suspend fun startQuizSession(quizSession: QuizSession) : QuizSession {
        return quizSessionDao.startQuizSession(quizSession)
    }

    /**
     * Update the score of the current quiz session
     */
    suspend fun updateScore(quizSession: QuizSession){
        quizSessionDao.updateScore(quizSession)
    }

    /**
     * End the current quiz session
     */
    suspend fun endQuizSession(quizSession: QuizSession) {
        return quizSessionDao.endQuizSession(quizSession)
    }
}