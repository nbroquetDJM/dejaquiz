package fr.dejamobile.nbroquet.dejaquiz.model

import android.os.Parcel
import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

/**
 * Response entity from the database
 */
@Entity(
    tableName = "Response",
    foreignKeys = [
        ForeignKey(entity = Question::class, parentColumns = ["id"], childColumns = ["questionId"])
    ],
    indices = [Index("questionId")]
)
@Parcelize
data class Response(@PrimaryKey(autoGenerate = true) val id: Int, val response: String, val questionId: Int): Parcelable {
    /**
     * Compare items for the recyclerview
     */
    class DiffCallback: DiffUtil.ItemCallback<Response>() {
        override fun areItemsTheSame(oldItem: Response, newItem: Response): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Response, newItem: Response): Boolean {
            return oldItem.id == newItem.id && oldItem.response == newItem.response && oldItem.questionId == newItem.questionId
        }
    }

    companion object {
        val EMPTY_RESPONSE = Response(0, "", 0)
    }
}